package com.example.sandstranger.testdrawable.ui.activity;

import android.support.v4.app.Fragment;

import com.androidutils.ui.activity.SingleActivity;
import com.example.sandstranger.testdrawable.ui.fragment.DrawFragment;

public class DrawActivity extends SingleActivity {

    @Override
    protected Fragment createFragment() {
        return DrawFragment.newInstance();
    }
}
