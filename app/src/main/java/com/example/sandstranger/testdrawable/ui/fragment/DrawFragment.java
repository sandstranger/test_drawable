package com.example.sandstranger.testdrawable.ui.fragment;

import android.support.v4.app.Fragment;
import android.view.View;

import com.androidutils.ui.fragment.SingleFragment;
import com.example.sandstranger.testdrawable.R;

public class DrawFragment extends SingleFragment {
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_draw;
    }

    @Override
    protected void bindView(View view) {

    }

    public static Fragment newInstance (){
        return new DrawFragment();
    }
}
