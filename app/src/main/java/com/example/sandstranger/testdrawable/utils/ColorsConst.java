package com.example.sandstranger.testdrawable.utils;

public final class ColorsConst {

    public static final int RED_TRANSPARENT_COLOR;
    public static final int GRAY_TRANSPARENT_COLOR;

    private ColorsConst() {

    }

    static {
        RED_TRANSPARENT_COLOR = 0x22ff0000;
        GRAY_TRANSPARENT_COLOR = 0xfff8efe0;
    }
}
