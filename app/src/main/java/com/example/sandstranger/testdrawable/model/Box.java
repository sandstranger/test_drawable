package com.example.sandstranger.testdrawable.model;

import android.graphics.PointF;

import lombok.Getter;
import lombok.Setter;

public class Box {
    @Getter
    private final PointF originPoint;
    @Getter
    @Setter
    private PointF currentPoint;

    public Box(PointF originPoint) {
        this.originPoint = originPoint;
        this.currentPoint = originPoint;
    }
}
