package com.example.sandstranger.testdrawable.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.example.sandstranger.testdrawable.R;
import com.example.sandstranger.testdrawable.model.Box;
import com.example.sandstranger.testdrawable.utils.ColorsConst;

import java.util.ArrayList;
import java.util.List;

import lombok.val;

public class BoxDrawingView extends View {

    private Box currentBox = null;
    private final List<Box> boxes = new ArrayList<>();
    private final Paint boxPaint = new Paint();
    private final Paint backgroundPaint = new Paint();

    public BoxDrawingView(Context context) {
        super(context);
        initPaint();
    }

    public BoxDrawingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    private void initPaint() {
        boxPaint.setColor(ColorsConst.RED_TRANSPARENT_COLOR);
        backgroundPaint.setColor(ColorsConst.GRAY_TRANSPARENT_COLOR);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        PointF originPoint = new PointF(event.getX(), event.getY());
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                currentBox = new Box(originPoint);
                boxes.add(currentBox);
                break;
            case MotionEvent.ACTION_MOVE:
                if (currentBox != null) {
                    currentBox.setCurrentPoint(originPoint);
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                currentBox = null;
                break;

        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPaint(backgroundPaint);
        drawBoxes(canvas);
    }

    private void drawBoxes(Canvas canvas) {
        for (val box : boxes) {
            drawBox(box, canvas);
        }
    }

    private void drawBox(Box box, Canvas canvas) {
        val left = Math.min(box.getOriginPoint().x, box.getCurrentPoint().x);
        val right = Math.max(box.getOriginPoint().x, box.getCurrentPoint().x);
        val top = Math.min(box.getOriginPoint().y, box.getCurrentPoint().y);
        val bottom = Math.max(box.getOriginPoint().y, box.getCurrentPoint().y);
        canvas.drawRect(left, top, right, bottom, boxPaint);
    }
}
